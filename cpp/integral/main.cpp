#include <math.h>
#include <iostream>

using namespace std;

const double ZERO = 0.;
const double DEFAULT_MIN_LIMIT = -5.;
const double DEFAULT_MAX_LIMIT = 5.;
const double DEFAULT_STEP = 0.1;

struct CartesianPoint {
    double x;
    double y;
    double z;
};

CartesianPoint getZeroPoint() {
    CartesianPoint point;
    point.x = ZERO;
    point.x = ZERO;
    point.x = ZERO;
    return point;
}

CartesianPoint createPoint(double x, double y, double z) {
    CartesianPoint point;
    point.x = x;
    point.y = y;
    point.z = z;
    return point;
}

void printCartesianPoint(const char* name, CartesianPoint point) {
    cout << "CartesianPoint(" << name << ", x = " << point.x << \
        ", y = " << point.y << ", z = " << point.z << ")\n";
}

struct IntegrationLimits {
    double minValue;
    double maxValue;
};

IntegrationLimits getDefaultIntegrationLimits() {
    IntegrationLimits limits;
    limits.minValue = DEFAULT_MIN_LIMIT;
    limits.maxValue = DEFAULT_MAX_LIMIT;
    return limits;
}

void printIntegrationLimits(const char* name, IntegrationLimits limits) {
    cout << "IntegrationLimits for " << name << " minValue = " << \
        limits.minValue << " maxValue = " << limits.maxValue << endl;
}

double gaussFunction(double alpha, CartesianPoint center, CartesianPoint point) {
    if (alpha < 0) {
        alpha = -alpha;
    }
    double x = center.x - point.x;
    double y = center.y - point.y;
    double z = center.z - point.z;
    double square_r = (x*x + y*y + z*z);
    double result = exp(-alpha * square_r);
    return result;
}

double integrateGaussFunction(double alpha, CartesianPoint center, double step = DEFAULT_STEP) {
    double result = ZERO;
    IntegrationLimits xLimits = getDefaultIntegrationLimits();
    IntegrationLimits yLimits = getDefaultIntegrationLimits();
    IntegrationLimits zLimits = getDefaultIntegrationLimits();
    double stepX = step; // dx
    double stepY = step; // dy
    double stepZ = step; // dz
    cout << "\nStrat integrating for params:\n";
    cout << "alpha = " << alpha << endl;
    printCartesianPoint("center", center);
    cout << "step = " << step << endl;
    printIntegrationLimits("x", xLimits);
    printIntegrationLimits("y", yLimits);
    printIntegrationLimits("z", zLimits);
    double volumeElement = stepX * stepY * stepZ; // dV
    cout << "volumeElement = " << volumeElement << endl;
    for(double x = xLimits.minValue; x < xLimits.maxValue; x += stepX) {
        for(double y = yLimits.minValue; y < yLimits.maxValue; y += stepY) {
            for(double z = zLimits.minValue; z < zLimits.maxValue; z += stepZ) {
                CartesianPoint point = createPoint(x, y, z);
                result += gaussFunction(alpha, center, point) * volumeElement;
            }
        }
    }
    cout << "Done!\n";
    cout << "\tIntegral = " << result << "\n\n";
    return result;
}

void showDifferentAlphas() {
    double alpha1 = 1.;
    double alpha2 = 5.;
    CartesianPoint zeroCenter = getZeroPoint();
    cout << "\nDifferentAlphas <<\n";
    integrateGaussFunction(alpha1, zeroCenter);
    integrateGaussFunction(alpha2, zeroCenter);
    cout << ">>\n\n";
}

void showDifferentCenters() {
    double alpha = 1.;
    CartesianPoint zeroCenter = getZeroPoint();
    CartesianPoint otherCenter = createPoint(1., -1., M_PI);
    cout << "\nDifferentCenters <<\n";
    integrateGaussFunction(alpha, zeroCenter);
    integrateGaussFunction(alpha, otherCenter);
    cout << ">>\n\n";
}

int main(int argn, char** argv) {
    showDifferentAlphas();
    showDifferentCenters();
    return 0;
}

